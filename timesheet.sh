#!/bin/bash

output_file=TIMESHEET.md

time=`git log | awk '{match($0, /:stopwatch: [0-9]+/); pattern=substr($0, RSTART, RLENGTH); sub(/:stopwatch: /, "", pattern); time+=pattern; $0=substr($0, RSTART, RLENGTH)} END {print time}'`

((h = $time / 60))
((m = $time - $h * 60))

[[ $h -gt 0 ]] && total=${h}h

if [[ $m -gt 0 ]]; then
  [[ $h -gt 0 ]] && total=$total" "
  total=$total${m}m
fi

[[ -z $total ]] && exit

echo "**Syntax for commit message:**\`<ignored_text> :stopwatch: <value> <ignored text>\`" > $output_file
echo >> $output_file
echo "\`<value>\` represents minutes (e.g. \`Fixes #1 :stopwatch: 120 minutes)\`" >> $output_file
echo >> $output_file
echo "Usage: It is better to include the time logging text at the end of the commit message." >> $output_file
echo >> $output_file
echo "---" >> $output_file
echo >> $output_file
echo "## Total time spent on tasks: $total" >> $output_file
echo >> $output_file
echo "---" >> $output_file
echo >> $output_file

no_commits=`git rev-list --count HEAD`
for (( i=0; i<$no_commits; i++ )); do
  partial_time=
  partial_h=
  partial_m=
  partial_total=

  partial_time=`git log --pretty="%B" -1 --skip=$i | awk '{match($0,":stopwatch: ([0-9]+)",t); time+=t[1]} END {print time}'`
  [[ $partial_time -eq 0 ]] && continue

  ((partial_h = $partial_time / 60))
  ((partial_m = $partial_time - $partial_h * 60))

  [[ $partial_h -gt 0 ]] && partial_total=${partial_h}h

  if [[ $partial_m -gt 0 ]]; then
    [[ $partial_h -gt 0 ]] && partial_total=$partial_total" "
    partial_total=$partial_total${partial_m}m
  fi

  [[ -z $partial_total ]] && continue

  commit_author=`git log --pretty="%an" -1 --skip=$i`
  commit_hash=`git log --pretty="%h" -1 --skip=$i`

  echo ":stopwatch: $partial_total - $commit_author - $commit_hash" >> $output_file
  echo >> $output_file
  git log --pretty="%B" -1 --skip=$i | awk '!/^:stopwatch:|^$/' >> $output_file
  echo >> $output_file
  echo "---" >> $output_file
done
